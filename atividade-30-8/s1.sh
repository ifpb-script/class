#!/bin/bash

access_baixar() {
	if [ ! -f "access.log" ]; then
		echo Realizando download do arquivo "access.log"
		wget https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log
	else	
		echo Arquivo "access.log" já baixado!
	fi
}

access_baixar
