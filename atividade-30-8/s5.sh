#!/bin/bash

while true; do
	echo -e "\nEscolha qual opção:\n"
	echo [1] Efetuar Download
	echo [2] Exibir Resumo do Arquivo
	echo [3] Listar IPs
	echo [4] Exibir IP aleatório
	echo -e "[5] Sair\n"
	read a
	if [ $a == 5 ]; then
		break
	fi
	if [ $a == 1 ]; then
		./s1.sh
	fi
	if [ $a == 2 ]; then
		./s2.sh
	fi
	if [ $a == 3 ]; then
		./s3.sh
	fi
	if [ $a == 4 ]; then
		./s4.sh
	fi
done
