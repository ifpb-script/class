#!/bin/bash

tr ' ' '_' < $1 > rs.txt

while IFS= read -r line; do
	touch "$line"
done < rs.txt

rm rs.txt
