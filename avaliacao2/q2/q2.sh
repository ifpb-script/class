#!/bin/bash


bk="/tmp/backup"
mkdir -p "$bk"

us="/home/$(whoami)"
nm="backup_$(date '+%H%M').tar.gz"

cp -R "$us" "$bk"
tar -czf "$bk/$nm" -C "$bk" "$(basename "$us")"
rm -rf "$bk/$(whoami)"

echo "Seu backup foi finalizado boy!"
