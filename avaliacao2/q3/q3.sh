#!/bin/bash

show_dialog() {
    dt=$(yad --calendar --title="Agendar Backup" --text="Escolha a data e hora para o backup:" --center --width=300 --height=300 --date-format="%Y-%m-%d %H:%M" --borders=10)
    echo "$dt"
}

dt=$(show_dialog)

if [ -z "$dt" ]; then
    echo "Nenhuma data e hora selecionada. Backup cancelado."
    exit 1
fi

bd=$(date -d "$datetime" '+%Y-%m-%d %H:%M')

bk="/tmp/backup"
mkdir -p "$bk"

us="/home/$(whoami)"

nm="backup_$(date '+%H%M').tar.gz"

cp -R "$us" "$bk"

tar -czf "$bk/$nm" -C "$bk" "$(basename "$us")"

rm -rf "$bk/$(whoami)"

echo "$bd $PWD/$(basename "$0")" | at "$bd"

yad --title="Backup Agendado" --text="Backup agendado com sucesso!" --button="OK:0" --center
