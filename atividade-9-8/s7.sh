#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Uso: $0 <a> <b>"
    exit 1
fi

a=$1
b=$2

if ! [[ "$a" =~ ^-?[0-9]+$ ]] || ! [[ "$b" =~ ^-?[0-9]+$ ]]; then
    echo "Os parâmetros devem ser números inteiros."
    exit 1
fi

if [ "$a" -gt "$b" ]; then
    echo "O valor de a deve ser menor ou igual ao valor de b."
    exit 1
fi

for ((i=a; i<=b; i++)); do
    if (( i % 2 == 0 )); then
        echo "$i"
    fi
done

