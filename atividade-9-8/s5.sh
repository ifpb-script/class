#!/bin/bash

echo "Ajuda sobre redirecionadores:"
echo

echo "Redirecionador: >"
echo "Descrição: Redireciona a saída padrão (stdout) para um arquivo, sobrescrevendo o arquivo se ele já existir."
echo "Exemplo: echo 'Olá Mundo' > arquivo.txt"
echo

echo "Redirecionador: >>"
echo "Descrição: Redireciona a saída padrão (stdout) para um arquivo, adicionando o conteúdo ao final do arquivo se ele já existir."
echo "Exemplo: echo 'Outra linha' >> arquivo.txt"
echo

echo "Redirecionador: 2>"
echo "Descrição: Redireciona a saída de erro padrão (stderr) para um arquivo, sobrescrevendo o arquivo se ele já existir."
echo "Exemplo: ls /diretorio_inexistente 2> erro.txt"
echo

echo "Redirecionador: 2>>"
echo "Descrição: Redireciona a saída de erro padrão (stderr) para um arquivo, adicionando o conteúdo ao final do arquivo se ele já existir."
echo "Exemplo: ls /diretorio_inexistente 2>> erro.txt"
echo


echo "Redirecionador: &>"
echo "Descrição: Redireciona tanto a saída padrão (stdout) quanto a saída de erro padrão (stderr) para um arquivo, sobrescrevendo o arquivo se ele já existir."
echo "Exemplo: ls /diretorio_inexistente &> saida_e_erro.txt"
echo


echo "Redirecionador: &>>"
echo "Descrição: Redireciona tanto a saída padrão (stdout) quanto a saída de erro padrão (stderr) para um arquivo, adicionando o conteúdo ao final do arquivo se ele já existir."
echo "Exemplo: ls /diretorio_inexistente &>> saida_e_erro.txt"
echo

echo "Redirecionador: <"
echo "Descrição: Redireciona a entrada padrão (stdin) de um arquivo."
echo "Exemplo: sort < lista.txt"
echo

echo "Redirecionador: <<"
echo "Descrição: Redireciona a entrada padrão (stdin) a partir de um bloco de texto até um delimitador específico."
echo "Exemplo: cat <<EOF\nLinha 1\nLinha 2\nEOF"
echo


echo "Redirecionador: <<<"
echo "Descrição: Redireciona a entrada padrão (stdin) a partir de uma string literal."
echo "Exemplo: grep 'texto' <<< 'texto que será pesquisado'"
echo

echo "Redirecionador: |"
echo "Descrição: Redireciona a saída padrão (stdout) de um comando para a entrada padrão (stdin) de outro comando."
echo "Exemplo: ls | grep 'txt'"
echo

