#!/bin/bash

for a in *; do
	if [ -f "$a" ]; then
		l=$(wc -l < $a)
		echo "O arquivo $a tem $l linhas"
	fi
done
