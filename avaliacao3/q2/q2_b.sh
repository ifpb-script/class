#!/bin/bash

if [ -f access.log ]; then
	cat access.log | cut -d ' ' -f 1 | sort -nu
else
	echo Arquivo não existe! Execute o q2_a.sh!
fi
