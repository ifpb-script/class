#!/bin/bash

echo Escolha um número entre 1 e 100 para realizar a requisição dos dados dos usuários:
read num

echo -e "\nRealizando download\n!"
wget -q https://dummyjson.com/user/$num
cat $num | cut -d ',' -f 2 | cut -d ':' -f 2 | tr -d '"' > n1.txt
cat $num | cut -d ',' -f 3 | cut -d ':' -f 2 | tr -d '"' > n2.txt
paste n1.txt n2.txt > t.txt
rm n1.txt
rm n2.txt

cat $num | cut -d ',' -f 9 | cut -d ':' -f 2 | tr -d '"' >> t.txt
cat $num | cut -d ',' -f 10 | cut -d ':' -f 2 | tr -d '"' >> t.txt
cat $num | cut -d ',' -f 19 | cut -d ':' -f 2 | tr -d '"' >> t.txt
cat $num | cut -d ',' -f 7 | cut -d ':' -f 2 | tr -d '"' >> t.txt

echo -e "\nDados do usuário gerado e salvo em dados.txt:\n"
cat t.txt | tr '\n' '::'
cat t.txt | tr '\n' '::' > dados.txt
rm t.txt

