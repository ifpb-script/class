#!/bin/bash

while IFS=: read -r grp nam gid mem; do
	grp=$(echo "$grp" | cut -d':' -f1)
    	nam=$(echo "$nam" | cut -d':' -f2)
    	gid=$(echo "$gid" | cut -d':' -f3)
    	mem=$(echo "$mem" | cut -d':' -f4)
	
	echo "$mem:$gid:$nam:$grp"
done < /etc/group
