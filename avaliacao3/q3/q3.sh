#!/bin/bash

cnt=1
ent=$1
atv=""
pre="p"

while IFS= read -r lin; do
	if [ "$lin" = "----" ]; then
		if [ -n "$atv" ]; then
			sai="${pre}${cnt}.txt"
            		echo "$atv" | tr '|' '\n' > "$sai"
            		echo "Parte $cnt escrita em $sai"
            		cnt=$((cnt + 1))
            		atv=""
		fi
	else
		atv="$atv$lin|"
	fi
done < $ent

if [ -n "$atv" ]; then
	sai="${pre}${cnt}.txt"
	echo -e $atv > $sai
	echo "Parte $cnt escrita em $sai"
fi
